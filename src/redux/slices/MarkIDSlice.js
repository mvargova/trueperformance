import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: 0,
};

export const MarkIDSlice = createSlice({
  name: "markId",
  initialState,
  reducers: {
    genNextID: (state) => {
      state.value += 1;
    },
  },
});

export const { genNextID } = MarkIDSlice.actions;

export default MarkIDSlice.reducer;
