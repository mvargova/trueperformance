import "./App.css";
import React, { useEffect } from "react";
import NavBar from "./component/navbar/NavBar";
import Game from "./component/game/Game";
import Players from "./component/players/Players";
import { fillGlobalInfo } from "./redux/slices/GlobalInfoSlice";
import { setTeam } from "./redux/slices/MarkInfoSlice";
// selector for getting state, dispatch for dispatching actions to reducers (editing state)
import { useDispatch } from "react-redux";

import "bootstrap/dist/css/bootstrap.min.css";
import SettingsModal from "./component/modals/SettingsModal";
import ColorPalet from "./component/ColorPalet";

function App() {
  const dispatch = useDispatch();

  // nastavenie hodnot do marky
  useEffect(() => {
    dispatch(
      fillGlobalInfo({
        gGuestTeamLongName: "Finland",
        gGuestTeamShortName: "FIN",
        gHomeTeamLongName: "Slovakia",
        gHomeTeamShortName: "SVK",
        gCategory: "U20",
        gDateTime: new Date().toISOString(),
        mTeam: "SVK",
        teamOnLeft: "SVK",
        teamOnRight: "FIN",
      })
    );
  }, []);

  return (
    <div className="App">
      {/* <SettingsModal
        show={showSettingsModal}
        handleClose={() => setShowSettingsModal(false)}
      /> */}
      <div className="navBar-wrapper">
        <NavBar />
      </div>
      <div className="content-wrapper">
        <Game />
        <ColorPalet />
        <Players />
      </div>

      {/* <button onClick={() => setShowSettingsModal(!showSettingsModal)}>
        change show modal
      </button> */}
    </div>
  );
}

export default App;
