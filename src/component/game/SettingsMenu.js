import React from "react";

import { Dropdown, DropdownButton } from "react-bootstrap";

const SettingsMenu = () => {
  return (
    <DropdownButton id="dropdown-button" title="...">
      <Dropdown.Item href="#/action-1">New Match</Dropdown.Item>
      <Dropdown.Item href="#/action-2">Open Match</Dropdown.Item>
      <Dropdown.Item href="#/action-3">Save Match</Dropdown.Item>
      <Dropdown.Item href="#/action-3">Find and Replace</Dropdown.Item>
      <Dropdown.Item href="#/action-3">Create a Synchro tag (F4)</Dropdown.Item>
    </DropdownButton>
  );
};

export default SettingsMenu;
