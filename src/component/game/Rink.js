import React from "react";
import iceRink from "../../images/iceRink.png";
import PuckHolder from "./PuckHolder";

function Rink() {
  // string representing puck position from left A-I
  const [puckPosition, setPuckPosition] = React.useState("E");

  return (
    <div className="rink">
      <div style={{ position: "relative" }}>
        <img id="main-image" src={iceRink} width="100%" alt="ice rink" />

        <PuckHolder
          holderPosition="I"
          currentPuckPosition={puckPosition}
          setPuckPosition={setPuckPosition}
          style={{
            width: "14.5%",
            height: "29.0%",
            left: "74.5%",
            top: "56.7%",
          }}
        />

        <PuckHolder
          holderPosition="H"
          currentPuckPosition={puckPosition}
          setPuckPosition={setPuckPosition}
          style={{
            width: "14.5%",
            height: "29.0%",
            left: "74.5%",
            top: "13.5%",
          }}
        />

        <PuckHolder
          holderPosition="F"
          currentPuckPosition={puckPosition}
          setPuckPosition={setPuckPosition}
          style={{
            width: "14.5%",
            height: "29.0%",
            left: "54.4%",
            top: "13.5%",
          }}
        />

        <PuckHolder
          holderPosition="G"
          currentPuckPosition={puckPosition}
          setPuckPosition={setPuckPosition}
          style={{
            width: "14.5%",
            height: "29.0%",
            left: "54.4%",
            top: "56.7%",
          }}
        />

        <PuckHolder
          holderPosition="E"
          currentPuckPosition={puckPosition}
          setPuckPosition={setPuckPosition}
          style={{
            width: "14.5%",
            height: "29%",
            left: "42.8%",
            top: "35.3%",
          }}
        />

        <PuckHolder
          holderPosition="D"
          currentPuckPosition={puckPosition}
          setPuckPosition={setPuckPosition}
          style={{
            width: "14.5%",
            height: "29%",
            left: "31.8%",
            top: "56.7%",
          }}
        />

        <PuckHolder
          holderPosition="C"
          currentPuckPosition={puckPosition}
          setPuckPosition={setPuckPosition}
          style={{
            width: "14.5%",
            height: "29%",
            left: "31.9%",

            top: "13.5%",
          }}
        />

        <PuckHolder
          holderPosition="A"
          currentPuckPosition={puckPosition}
          setPuckPosition={setPuckPosition}
          style={{
            width: "14.5%",
            height: "29%",
            left: "11.4%",
            top: "13.5%",
          }}
        />

        <PuckHolder
          holderPosition="B"
          currentPuckPosition={puckPosition}
          setPuckPosition={setPuckPosition}
          style={{
            width: "14.5%",
            height: "29.0%",
            left: "11.2%",
            top: "56.7%",
          }}
        />

        <p
          className="rim-hotspot"
          style={{
            position: "absolute",
            cursor: "pointer",
            display: "block",
            zIndex: 5,
            overflow: "hidden",
            textAlign: "center",
            color: "rgba(0,0,0,0.5)",
            width: "8.1%",
            height: "15.6%",
            left: "45.8%",
            top: "81.8%",
          }}
        >
          {puckPosition}
        </p>
      </div>
    </div>
  );
}

export default Rink;
