import React from "react";
import puck from "../../images/puck.png";

export default function PuckHolder(props) {
  const { setPuckPosition, holderPosition, currentPuckPosition, style } = props;

  return (
    <div>
      <a
        className="rim-hotspot"
        style={{
          ...style,
          position: "absolute",
          cursor: "pointer",
          display: "block",
          zIndex: 5,
          overflow: "hidden",
          backgroundImage:
            holderPosition === currentPuckPosition ? `url(${puck})` : "none",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          backgroundSize: "2rem",
        }}
        onClick={() => setPuckPosition(holderPosition)}
      ></a>
    </div>
  );
}
