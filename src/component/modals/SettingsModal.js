import React from "react";
import { Modal, Button } from "react-bootstrap";

const SettingsModal = (props) => {
  const { show, handleClose } = props;
  return (
    <Modal
      show={show}
      size="lg"
      centered
      onEscapeKeyDown={() => handleClose()}
      onHide={() => handleClose()}
      contentClassName="settings"
      className="settings-modal"
    >
      <Modal.Header closeButton>
        <Modal.Title>Modal heading</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>Woohoo, you're reading this text in a modal! </div>
        <div>Woohoo, you're reading this text in a modal! </div>
        <div>Woohoo, you're reading this text in a modal! </div>
        <div>Woohoo, you're reading this text in a modal! </div>
        <div>Woohoo, you're reading this text in a modal! </div>
        <div>Woohoo, you're reading this text in a modal! </div>
        <div>Woohoo, you're reading this text in a modal! </div>
        <div>Woohoo, you're reading this text in a modal! </div>
        <div>Woohoo, you're reading this text in a modal! </div>

        <div>Woohoo, you're reading this text in a modal! </div>
        <div>Woohoo, you're reading this text in a modal! </div>
        <div>Woohoo, you're reading this text in a modal! </div>
        <div>Woohoo, you're reading this text in a modal! </div>
        <div>Woohoo, you're reading this text in a modal! </div>
        <div>Woohoo, you're reading this text in a modal! </div>
        <div>Woohoo, you're reading this text in a modal! </div>

        <div>Woohoo, you're reading this text in a modal! </div>
      </Modal.Body>
      <Modal.Footer
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
        <Button
          bsPrefix="save-modal-btn"
          variant=""
          onClick={() => alert("savnute")}
        >
          Save Changes
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default SettingsModal;
