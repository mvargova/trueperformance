import PlayersNumber from "./PlayersNumber";
import { Tabs, Tab } from "react-bootstrap";
import GoaliePlayers from "./GoaliePlayers";
//import PlayersNumberR from "./PlayersNumberR";

function Players() {
  return (
    <div className="players-wrapper">
      <Tabs
        defaultActiveKey="home"
        id="guest-home-tabs"
        style={{
          border: 0,
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Tab eventKey="home" title="HOME">
          <div className="players-number-content" style={{ minHeight: "100%" }}>
            <div className="players-content">
              <PlayersNumber numberLine={"L1"} numberF={"F1"} numberD={"D1"} />
              <PlayersNumber numberLine={"L2"} numberF={"F2"} numberD={"D2"} />
              <PlayersNumber numberLine={"L3"} numberF={"F3"} numberD={"D3"} />
              <PlayersNumber numberLine={"L4"} numberF={"F4"} numberD={"D4"} />
              <PlayersNumber numberLine={"L5"} numberF={"F5"} numberD={"D5"} />
            </div>
            {/* <div className="players-goalie">
              <GoaliePlayers />
            </div> */}
          </div>
        </Tab>
        <Tab eventKey="guest" title="GUEST">
          <div className="players-number-content">
            <div className="players-content">
              <PlayersNumber />
              <PlayersNumber />
              <PlayersNumber />
              <PlayersNumber />
              <PlayersNumber />
            </div>
            {/* <div className="players-goalie">
              <GoaliePlayers />
            </div> */}
          </div>
        </Tab>
      </Tabs>

      <div className="players-button">
        {/* <button className="buttonButton">
          Undo latest
          <br /> players
        </button> */}
        <button className="buttonButton">
          Close all active <br /> players (F7)
        </button>
        <button className="buttonButton">
          Start for active <br /> players (F6)
        </button>
      </div>
    </div>
  );
}

export default Players;
