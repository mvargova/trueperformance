import LayoutPlayers from "./LayoutPlayers";

function GoaliePlayers() {
  return (
    <div
      className="goalie-players"
      // style={{
      //   display: "flex",
      //   flexDirection: "column",

      //   width: "100%",
      //   height: "100%",
      //   justifyContent: "flex-end",
      // }}
    >
      <LayoutPlayers
        number={68}
        position={"G"}
        line={"L1"}
        style={{ marginBottom: "0.5rem", height: "9%" }}
      />
      <LayoutPlayers
        number={72}
        position={"G"}
        line={"L1"}
        style={{ marginBottom: "0.5rem", height: "9%" }}
      />
      <LayoutPlayers
        number={91}
        position={"G"}
        line={"L1"}
        style={{ marginBottom: "0.3rem", height: "9%" }}
      />
    </div>
  );
}

export default GoaliePlayers;
