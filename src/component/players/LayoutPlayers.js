import { buttonsData } from "../data/ButtonsData";

const LayoutPlayers = (props) => {
  const { number, position, line, style } = props;

  return (
    <button className="up2" style={style}>
      {number}
    </button>
  );
};
export default LayoutPlayers;
