import LayoutPlayers from "./LayoutPlayers";

function PlayersNumber(props) {
  const { numberLine, numberF, numberD } = props;
  return (
    <div className="players-number-L">
      <div className="players-number-L1">
        <button className="L1">{numberLine}</button>
      </div>
      <div className="players-number-F1D1">
        <button className="F1">{numberF}</button>
        <button className="D1">{numberD}</button>
      </div>
      <div className="players-number">
        {/* <div className="players-number-up"> */}
        <div
          className="players-layout-number"
          // style={{
          //   display: "grid",
          //   gridTemplateColumns: "repeat(12, 1fr)",
          //   gridTemplateRows: "repeat(2, 1fr)",
          //   width: "100%",
          //   height: "100%",
          //   columnGap: "0.5rem",
          //   rowGap: "0.5rem",
          //   paddingLeft: "0.5rem",
          //   placeItems: "center",
          // }}
        >
          <LayoutPlayers number={2} position={"LW"} line={"L1"} />
          <LayoutPlayers number={3} position={"CE"} line={"L1"} />
          <LayoutPlayers number={5} position={"RW"} line={"L1"} />
          <LayoutPlayers number={7} position={"LD"} line={"L1"} />
          <LayoutPlayers number={9} position={"RD"} line={"L1"} />
        </div>
      </div>
    </div>
  );
}

export default PlayersNumber;
