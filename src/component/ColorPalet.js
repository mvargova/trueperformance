import React from "react";

const ColorPalet = () => {
  return (
    <div className="color-wrapper">
      <button className="color" style={{ backgroundColor: "#00ffff" }}></button>
      <button className="color" style={{ backgroundColor: "#0000ff" }}></button>
      <button className="color" style={{ backgroundColor: "#8a2be2" }}></button>
      <button className="color" style={{ backgroundColor: "#ee82ee" }}></button>
      <button className="color" style={{ backgroundColor: "#ff0000" }}></button>
      <button className="color" style={{ backgroundColor: "#ffa500" }}></button>
      <button className="color" style={{ backgroundColor: "#ffff00" }}></button>
      <button className="color" style={{ backgroundColor: "#adff2f" }}></button>
      <button className="color" style={{ backgroundColor: "#228b22" }}></button>
    </div>
  );
};

export default ColorPalet;
